package mr.gov.aspadbackend.plan_action.repositories;

import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.plan_action.entites.Axes;
import mr.gov.aspadbackend.plan_action.entites.Objectifs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource
@CrossOrigin(origins = "*")
public interface ActivitesRepository extends JpaRepository<Activites, Long> {
    List<Activites> findAllByObjectifs(Objectifs objectifs);
}
