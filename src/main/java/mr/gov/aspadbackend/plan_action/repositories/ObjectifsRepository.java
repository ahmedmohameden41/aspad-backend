package mr.gov.aspadbackend.plan_action.repositories;

import mr.gov.aspadbackend.organigramme.entities.Instances;
import mr.gov.aspadbackend.plan_action.entites.Axes;
import mr.gov.aspadbackend.plan_action.entites.Objectifs;
import mr.gov.aspadbackend.plan_action.entites.PlanActions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource
@CrossOrigin(origins = "*")
public interface ObjectifsRepository extends JpaRepository<Objectifs, Long> {
    List<Objectifs> findAllByAxes(Axes axes);
}
