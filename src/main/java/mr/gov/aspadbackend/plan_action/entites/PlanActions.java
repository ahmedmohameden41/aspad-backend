package mr.gov.aspadbackend.plan_action.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import mr.gov.aspadbackend.organigramme.entities.Instances;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PlanActions implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long IdPlanAction;
	private String DesignationPlanAction;
	private String DescriptionPlanAction;
	private String StatutPlanAction;
//    @Size(max = 100, min = 0)
	private int PourcentagePlanAction;
	@Temporal(TemporalType.DATE)
	private Date DateDebutPlanAction;
	@Temporal(TemporalType.DATE)
	private Date DateFinPlanAction;
	@Temporal(TemporalType.DATE)
	private Date DateDebutPrevuePlanAction;
	@Temporal(TemporalType.DATE)
	private Date DateFinPrevuePlanAction;
	@OneToMany(mappedBy = "planActions", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	private Collection<Axes> axes;
	@ManyToOne
	private Instances instances;
	public Long getIdPlanAction() {
		return IdPlanAction;
	}
	public void setIdPlanAction(Long idPlanAction) {
		IdPlanAction = idPlanAction;
	}
	public String getDesignationPlanAction() {
		return DesignationPlanAction;
	}
	public void setDesignationPlanAction(String designationPlanAction) {
		DesignationPlanAction = designationPlanAction;
	}
	public String getDescriptionPlanAction() {
		return DescriptionPlanAction;
	}
	public void setDescriptionPlanAction(String descriptionPlanAction) {
		DescriptionPlanAction = descriptionPlanAction;
	}
	public String getStatutPlanAction() {
		return StatutPlanAction;
	}
	public void setStatutPlanAction(String statutPlanAction) {
		StatutPlanAction = statutPlanAction;
	}
	public int getPourcentagePlanAction() {
		return PourcentagePlanAction;
	}
	public void setPourcentagePlanAction(int pourcentagePlanAction) {
		PourcentagePlanAction = pourcentagePlanAction;
	}
	public Date getDateDebutPlanAction() {
		return DateDebutPlanAction;
	}
	public void setDateDebutPlanAction(Date dateDebutPlanAction) {
		DateDebutPlanAction = dateDebutPlanAction;
	}
	public Date getDateFinPlanAction() {
		return DateFinPlanAction;
	}
	public void setDateFinPlanAction(Date dateFinPlanAction) {
		DateFinPlanAction = dateFinPlanAction;
	}
	public Date getDateDebutPrevuePlanAction() {
		return DateDebutPrevuePlanAction;
	}
	public void setDateDebutPrevuePlanAction(Date dateDebutPrevuePlanAction) {
		DateDebutPrevuePlanAction = dateDebutPrevuePlanAction;
	}
	public Date getDateFinPrevuePlanAction() {
		return DateFinPrevuePlanAction;
	}
	public void setDateFinPrevuePlanAction(Date dateFinPrevuePlanAction) {
		DateFinPrevuePlanAction = dateFinPrevuePlanAction;
	}
	public Collection<Axes> getAxes() {
		return axes;
	}
	public void setAxes(Collection<Axes> axes) {
		this.axes = axes;
	}
	public Instances getInstances() {
		return instances;
	}
	public void setInstances(Instances instances) {
		this.instances = instances;
	}

}
