package mr.gov.aspadbackend.plan_action.entites;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Axes implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdAxe;
    private String DesignationAxe;
    private String DescriptionAxe;
    private String ObservationAxe;
    @ManyToOne
    private PlanActions planActions;
    @OneToMany(mappedBy = "axes", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Objectifs> objectifs;
}
