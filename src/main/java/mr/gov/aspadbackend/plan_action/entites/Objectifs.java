package mr.gov.aspadbackend.plan_action.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Objectifs implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdObjectif;
    private String DesignationObjectif;
    private String DescriptionObjectif;
    private String ObservationObjectif;
    @ManyToOne
    private Axes axes;
    @OneToMany(mappedBy = "objectifs", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Activites> activites;
}
