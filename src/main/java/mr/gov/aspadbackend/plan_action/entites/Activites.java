package mr.gov.aspadbackend.plan_action.entites;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.entities.Entites;
import mr.gov.aspadbackend.suivi_activites.entites.CommentairesActivite;
import mr.gov.aspadbackend.suivi_activites.entites.Taches;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalIdCache;
import org.hibernate.annotations.Cache;
import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@NaturalIdCache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Activites implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdActivite;
    private String DesignationActivite;
    private String DescriptionActivite;
    private String ObservationActivite;
    private String StatutActivite;
    @Temporal(TemporalType.DATE)
    private Date DateDebutActivite;
    @Temporal(TemporalType.DATE)
    private Date DateFinActivite;
    @Temporal(TemporalType.DATE)
    private Date DateDebutPrevueActivite;
    @Temporal(TemporalType.DATE)
    private Date DateFinPrevueActivite;
    @Size(max = 100, min = 0)
    private int PourcentageActivite;
    private String EtatInitialActivite;
    private String ObjectifActivite;
    private String FinancementActivite;
    private Long BudgetActivite;

    @ManyToOne
    private Objectifs objectifs;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(	name = "Entites_Activites",
            joinColumns = @JoinColumn(name = "IdActivite"),
            inverseJoinColumns = @JoinColumn(name = "IdEntite"))
    private Set<Entites> entites = new HashSet<>();

    @OneToMany(mappedBy = "activites", fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Taches> taches = new HashSet<>();;

    @OneToMany(mappedBy = "activites", fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<Collaborateurs> collaborateurs = new HashSet<>();

    @OneToMany(mappedBy = "activites",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<CommentairesActivite> commentairesActivites;
    
}

