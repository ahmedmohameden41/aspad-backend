package mr.gov.aspadbackend.plan_action.controllers;
import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.entities.Entites;
import mr.gov.aspadbackend.organigramme.entities.Instances;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.plan_action.entites.Axes;
import mr.gov.aspadbackend.plan_action.entites.Objectifs;
import mr.gov.aspadbackend.plan_action.entites.PlanActions;
import mr.gov.aspadbackend.plan_action.services.PlanActionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class PlanActionController {
    @Autowired
    private PlanActionsService planActionsService;



    @GetMapping(path = "/instanceses/{idInstance}/planActions")
    public List<PlanActions> getPlanActions(@PathVariable(name = "idInstance") Long idInstance){
        return planActionsService.getPlanActionsByInstance(idInstance);
    }
    @PostMapping(path = "/instanceses/{idInstance}/planActions")
    public PlanActions addNewPlanActions(@RequestBody PlanActions planActions, @PathVariable(name = "idInstance") Long idInstance){
        return planActionsService.addNewPlanActions(planActions, idInstance);
    }
    @PutMapping(path = "/planActionses/{idPlanAction}/{idInstance}")
    public PlanActions updatePlanActions(@RequestBody PlanActions planActions, @PathVariable(name = "idPlanAction") Long idPlanAction, @PathVariable(name = "idInstance") Long idInstance){
        return planActionsService.updatePlanActions(planActions, idPlanAction, idInstance);
    }


    @GetMapping(path = "/planActionses/{idPlanAction}/axes")
    public List<Axes> getAxes(@PathVariable(name = "idPlanAction") Long idPlanAction){
        return planActionsService.getAxesByPlanAction(idPlanAction);
    }
    @PostMapping(path = "/planActionses/{idPlanAction}/axes")
    public Axes addNewAxes(@RequestBody Axes axes, @PathVariable(name = "idPlanAction") Long idPlanAction){
        return planActionsService.addNewAxes(axes, idPlanAction);
    }
    @PutMapping(path = "/axeses/{idAxe}/{idPlanAction}")
    public Axes updateAxes(@RequestBody Axes axes, @PathVariable(name = "idAxe") Long idAxe, @PathVariable(name = "idPlanAction") Long idPlanAction){
        return planActionsService.updateAxes(axes, idAxe, idPlanAction);
    }


    @GetMapping(path = "/axeses/{idAxe}/objectifs")
    public List<Objectifs> getObjectifs(@PathVariable(name = "idAxe") Long idAxe){
        return planActionsService.getObjectifsByAxe(idAxe);
    }
    @PostMapping(path = "/axeses/{idAxe}/objectifs")
    public Objectifs addNewObjectis(@RequestBody Objectifs objectifs, @PathVariable(name = "idAxe") Long idAxe){
        return planActionsService.addNewObjectifs(objectifs, idAxe);
    }
    @PutMapping(path = "/objectifses/{idObjectif}/{idAxe}")
    public Objectifs updateAxes(@RequestBody Objectifs objectifs, @PathVariable(name = "idObjectif") Long idObjectif, @PathVariable(name = "idAxe") Long idAxe){
        return planActionsService.updateObjectifs(objectifs, idObjectif, idAxe);
    }

    @GetMapping(path = "/objectifses/{idObjectif}/activites")
    public List<Activites> getActivites(@PathVariable(name = "idObjectif") Long idObjectif){
        return planActionsService.getActivitesByObjectif(idObjectif);
    }
    @PostMapping(path = "/objectifses/{idObjectif}/activites")
    public Activites addNewActivites(@RequestBody Activites activites, @PathVariable(name = "idObjectif") Long idObjectif){
        return planActionsService.addNewActivites(activites, idObjectif);
    }
    @PutMapping(path = "/activiteses/{idActivite}/{idObjectif}/{idEntite}")
    public Activites updateActivites(@RequestBody Activites activites, @PathVariable(name = "idActivite") Long idActivite, @PathVariable(name = "idObjectif") Long idObjectif, @PathVariable(name = "idEntite") Long idEntite){
        return planActionsService.updateActivites(activites, idActivite, idObjectif, idEntite);
    }
    @PutMapping(path = "/activiteses/{idActivite}/{idObjectif}/{idEntite}/entites")
    public Activites updateEntiteActivite(@RequestBody Activites activites, @PathVariable(name = "idActivite") Long idActivite, @PathVariable(name = "idObjectif") Long idObjectif, @PathVariable(name = "idEntite") Long idEntite){
        return planActionsService.updateEntitesActivite(idActivite, idObjectif, idEntite);
    }
    @GetMapping(path = "/activiteses/{idActivite}/entites")
    public Set<Entites> getEntitesActivite(@PathVariable(name = "idActivite") Long idActivite){
        return planActionsService.getEntitesActivite(idActivite);
    }
    @GetMapping(path = "/activiteses/{idActivite}/collaborateurs")
    public Collection<Collaborateurs> getCollarateursActivite(@PathVariable(name = "idActivite") Long idActivite){
        return planActionsService.getCollarateursActivite(idActivite);
    }

}
