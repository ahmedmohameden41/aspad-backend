package mr.gov.aspadbackend.plan_action.services;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.entities.Entites;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.plan_action.entites.Axes;
import mr.gov.aspadbackend.plan_action.entites.Objectifs;
import mr.gov.aspadbackend.plan_action.entites.PlanActions;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface PlanActionsService {

    Activites addNewActivites(Activites activites, Long idObjectif);
    List<Activites> getActivitesByObjectif(Long idObjectif);
    Set<Entites> getEntitesActivite(Long idActivite);
    Collection<Collaborateurs> getCollarateursActivite(Long idActivite);
    Activites updateActivites(Activites activites, Long idActivite, Long idObjectif, Long idEntite);
    Activites updateEntitesActivite(Long idActivite, Long idObjectif, Long idEntite);

    Axes addNewAxes(Axes axes, Long idPlanAction);
    List<Axes> getAxesByPlanAction(Long idPlanAction);
    Axes updateAxes(Axes axes, Long idAxe, Long idPlanAction);

    Objectifs addNewObjectifs(Objectifs objectifs, Long idAxe);
    List<Objectifs> getObjectifsByAxe(Long idAxe);
    Objectifs updateObjectifs(Objectifs objectifs, Long idObjectif, Long idAxe);

    PlanActions addNewPlanActions(PlanActions planActions, Long idInstance);
    List<PlanActions> getPlanActionsByInstance(Long idInstance);
    PlanActions updatePlanActions(PlanActions planActions, Long idPlanAction, Long idInstance);


}
