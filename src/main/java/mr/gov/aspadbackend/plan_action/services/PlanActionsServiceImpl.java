package mr.gov.aspadbackend.plan_action.services;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.entities.Entites;
import mr.gov.aspadbackend.organigramme.entities.Instances;
import mr.gov.aspadbackend.organigramme.repositories.CollaborateursRepository;
import mr.gov.aspadbackend.organigramme.repositories.EntitesRepository;
import mr.gov.aspadbackend.organigramme.repositories.InstancesRepository;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.plan_action.entites.Axes;
import mr.gov.aspadbackend.plan_action.entites.Objectifs;
import mr.gov.aspadbackend.plan_action.entites.PlanActions;
import mr.gov.aspadbackend.plan_action.repositories.ActivitesRepository;
import mr.gov.aspadbackend.plan_action.repositories.AxesRepository;
import mr.gov.aspadbackend.plan_action.repositories.ObjectifsRepository;
import mr.gov.aspadbackend.plan_action.repositories.PlanActionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class PlanActionsServiceImpl implements PlanActionsService {
    @Autowired
    private PlanActionsRepository planActionsRepository;
    @Autowired
    private AxesRepository axesRepository;
    @Autowired
    private ObjectifsRepository objectifsRepository;
    @Autowired
    private ActivitesRepository activitesRepository;
    @Autowired
    private InstancesRepository instancesRepository;
    @Autowired
    private EntitesRepository entitesRepository;
    @Autowired
    private CollaborateursRepository collaborateursRepository;

    @Override
    public Activites addNewActivites(Activites activites, Long idObjectif) {
        Objectifs o = objectifsRepository.findById(idObjectif).get();
        activites.setObjectifs(o);
        return activitesRepository.save(activites);
    }

    @Override
    public List<Activites> getActivitesByObjectif(Long idObjectif) {
        Objectifs o = objectifsRepository.findById(idObjectif).get();
        return activitesRepository.findAllByObjectifs(o);
    }

    @Override
    public Set<Entites> getEntitesActivite(Long idActivite) {
        Activites a = activitesRepository.findById(idActivite).get();
        Set<Entites> entites = a.getEntites();
        return entites;
    }

    @Override
    public Collection<Collaborateurs> getCollarateursActivite(Long idActivite) {
        Activites a = activitesRepository.findById(idActivite).get();
        return a.getCollaborateurs();
    }

    @Override
    public Activites updateActivites(Activites activites, Long idActivite, Long idObjectif, Long idEntite) {
        if (idEntite != 0) {
            Entites e = entitesRepository.findById(idEntite).get();
            Activites a = activitesRepository.findById(idActivite).get();

            Collection<Collaborateurs> collaborateurs = e.getCollaborateurs();
            for (Collaborateurs c : collaborateurs) {
                c.setActivites(a);
            }

            Set<Entites> entites = a.getEntites();
            entites.add(e);
            a.setEntites(entites);
            return activitesRepository.save(a);

        } else {
            Objectifs o = objectifsRepository.findById(idObjectif).get();
            activites.setIdActivite(idActivite);
            activites.setObjectifs(o);
            return activitesRepository.save(activites);
        }
    }

    @Override
    public Activites updateEntitesActivite(Long idActivite, Long idObjectif, Long idEntite) {
        Entites e = entitesRepository.findById(idEntite).get();
        Activites a = activitesRepository.findById(idActivite).get();

        Collection<Collaborateurs> collaborateurs = e.getCollaborateurs();
        for (Collaborateurs c : collaborateurs) {
            c.setActivites(null);
            collaborateursRepository.save(c);
        }

        Set<Entites> entites = a.getEntites();
        entites.remove(e);
        a.setEntites(entites);
        return activitesRepository.save(a);
    }

    @Override
    public Axes addNewAxes(Axes axes, Long idPlanAction) {
        PlanActions p = planActionsRepository.findById(idPlanAction).get();
        axes.setPlanActions(p);
        return axesRepository.save(axes);
    }

    @Override
    public List<Axes> getAxesByPlanAction(Long idPlanAction) {
        PlanActions p = planActionsRepository.findById(idPlanAction).get();
        return axesRepository.findAllByPlanActions(p);
    }

    @Override
    public Axes updateAxes(Axes axes, Long idAxe, Long idPlanAction) {
        PlanActions p = planActionsRepository.findById(idPlanAction).get();
        axes.setIdAxe(idAxe);
        axes.setPlanActions(p);
        axes.setObjectifs(null);
        return axesRepository.save(axes);
    }

    @Override
    public Objectifs addNewObjectifs(Objectifs objectifs, Long idAxe) {
        Axes a = axesRepository.findById(idAxe).get();
        objectifs.setAxes(a);
        return objectifsRepository.save(objectifs);
    }

    @Override
    public List<Objectifs> getObjectifsByAxe(Long idAxe) {
        Axes a = axesRepository.findById(idAxe).get();
        return objectifsRepository.findAllByAxes(a);
    }

    @Override
    public Objectifs updateObjectifs(Objectifs objectifs, Long idObjectif, Long idAxe) {
        Axes a = axesRepository.findById(idAxe).get();
        objectifs.setIdObjectif(idObjectif);
        objectifs.setAxes(a);
        return objectifsRepository.save(objectifs);
    }

    @Override
    public PlanActions addNewPlanActions(PlanActions planActions, Long idInstance) {
        Instances i = instancesRepository.findById(idInstance).get();
        planActions.setInstances(i);
        return planActionsRepository.save(planActions);
    }

    @Override
    public List<PlanActions> getPlanActionsByInstance(Long idInstance) {
        Instances i = instancesRepository.findById(idInstance).get();
        return planActionsRepository.findAllByInstances(i);
    }

    @Override
    public PlanActions updatePlanActions(PlanActions planActions, Long idPlanAction, Long idInstance) {
        Instances i = instancesRepository.findById(idInstance).get();
        planActions.setIdPlanAction(idPlanAction);
        planActions.setInstances(i);
        return planActionsRepository.save(planActions);
    }

}
