package mr.gov.aspadbackend.referentiel.controllers;

import java.util.*;

import javax.validation.Valid;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.repositories.CollaborateursRepository;
import mr.gov.aspadbackend.referentiel.entities.ERole;
import mr.gov.aspadbackend.referentiel.entities.Role;
import mr.gov.aspadbackend.referentiel.entities.User;
import mr.gov.aspadbackend.referentiel.payload.request.LoginRequest;
import mr.gov.aspadbackend.referentiel.payload.request.SignupRequest;
import mr.gov.aspadbackend.referentiel.payload.response.JwtResponse;
import mr.gov.aspadbackend.referentiel.payload.response.MessageResponse;
import mr.gov.aspadbackend.referentiel.repositories.RoleRepository;
import mr.gov.aspadbackend.referentiel.repositories.UserRepository;
import mr.gov.aspadbackend.referentiel.security.jwt.JwtUtils;
import mr.gov.aspadbackend.referentiel.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	CollaborateursRepository collaborateursRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;


	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = new ArrayList<>();
		for (GrantedAuthority item : userDetails.getAuthorities()) {
			String authority = item.getAuthority();
			roles.add(authority);
		}

		return ResponseEntity.ok(new JwtResponse(jwt,
												 userDetails.getId(), 
												 userDetails.getUsername(), 
												 userDetails.getEmail(), 
												 roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageResponse("Error: Email is already in use!"));
		}
        try {
			Collaborateurs c = collaborateursRepository.findByEmailCollaborateur(signUpRequest.getEmail());
			c.setUser(true);
		}catch (Exception e){
			System.out.println(e);
		}
		// Create new user's account
		User user = new User(signUpRequest.getUsername(),
							 signUpRequest.getEmail(),
							 encoder.encode(signUpRequest.getPassword()));

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			Role decRole = roleRepository.findByName(ERole.ROLE_DECIDEUR)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(decRole);
		} else {
			for (String role : strRoles)
				switch (role) {
					case "admin":
						Role adminRole = roleRepository.findByName(ERole.ROLE_ADMINISTRATEUR)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(adminRole);
						//collaborateursRepository.save(new Collaborateurs(null, null,signUpRequest.getUsername(),null,signUpRequest.getEmail(), null,null,null,null,null,true, true,null, null, null, null, null, null));
						break;
					case "GESTIONNAIRE":
						Role gestionnaireRole = roleRepository.findByName(ERole.ROLE_GESTIONNAIRE)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(gestionnaireRole);
						break;
					case "utilisateur":
						Role userRole = roleRepository.findByName(ERole.ROLE_UTILISATEUR)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(userRole);
					default:
						Role decRole = roleRepository.findByName(ERole.ROLE_DECIDEUR)
								.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
						roles.add(decRole);

				}
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

}
