package mr.gov.aspadbackend.referentiel.repositories;

import java.util.Optional;

import mr.gov.aspadbackend.referentiel.entities.ERole;
import mr.gov.aspadbackend.referentiel.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*")
@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
}
