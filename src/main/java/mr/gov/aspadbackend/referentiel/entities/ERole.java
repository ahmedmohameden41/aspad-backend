package mr.gov.aspadbackend.referentiel.entities;

public enum ERole {
	ROLE_UTILISATEUR,
    ROLE_GESTIONNAIRE,
    ROLE_ADMINISTRATEUR,
    ROLE_DECIDEUR
}
