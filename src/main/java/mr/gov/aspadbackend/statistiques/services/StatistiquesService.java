package mr.gov.aspadbackend.statistiques.services;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.suivi_activites.entites.Taches;

import java.util.Collection;
import java.util.List;

public interface StatistiquesService {
   Collection<Collaborateurs> getCollaborateursByInstance(Long idInstance);

   Collection<Activites> getActivitesByInstance(Long idInstance);

   Collection<Taches> getTachesByInstance(Long idInstance);

   Collection<Activites> getActivitesByInstanceStatut(Long idInstance, String statut);

   Collection<Taches> getTachesByInstanceStatut(Long idInstance, String statut);
}
