package mr.gov.aspadbackend.statistiques.services;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.entities.Entites;
import mr.gov.aspadbackend.organigramme.entities.Instances;
import mr.gov.aspadbackend.organigramme.repositories.CollaborateursRepository;
import mr.gov.aspadbackend.organigramme.repositories.EntitesRepository;
import mr.gov.aspadbackend.organigramme.repositories.InstancesRepository;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.plan_action.entites.Axes;
import mr.gov.aspadbackend.plan_action.entites.Objectifs;
import mr.gov.aspadbackend.plan_action.entites.PlanActions;
import mr.gov.aspadbackend.plan_action.repositories.ActivitesRepository;
import mr.gov.aspadbackend.plan_action.repositories.PlanActionsRepository;
import mr.gov.aspadbackend.suivi_activites.entites.Taches;
import mr.gov.aspadbackend.suivi_activites.repositories.TachesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

@Service
@Transactional
public class StatistiquesServiceImpl implements StatistiquesService {
    @Autowired
    private InstancesRepository instancesRepository;
    @Autowired
    private EntitesRepository entitesRepository;

    @Override
    public Collection<Taches> getTachesByInstanceStatut(Long idInstance, String statut) {
        if(getTachesByInstance(idInstance)!=null){
            Collection<Taches> t = getTachesByInstance(idInstance);
            Collection<Taches> taches = new HashSet<>();
            for (Taches ta : t) {
                if (ta.getStatutTache().equals(statut)) {
                    taches.add(ta);
                }
            }
            return taches;
        }else return null;
    }

    @Override
    public Collection<Taches> getTachesByInstance(Long idInstance) {
        Collection<Activites> activites = getActivitesByInstance(idInstance);
        Collection<Taches> taches = new HashSet<>();
        activites.forEach(a -> taches.addAll(a.getTaches()));
        return taches;
    }

    @Override
    public Collection<Collaborateurs> getCollaborateursByInstance(Long idInstance) {
        Instances i = instancesRepository.findById(idInstance).get();
        List<Entites> entites = entitesRepository.findAllByInstances(i);
        Collection<Collaborateurs> collaborateurs=new HashSet<>();
        entites.forEach(e-> collaborateurs.addAll(e.getCollaborateurs()));
        return collaborateurs;
    }

    @Override
    public Collection<Activites> getActivitesByInstance(Long idInstance) {
        Instances i = instancesRepository.findById(idInstance).get();
        PlanActions p=new PlanActions();
        for (PlanActions planActions: i.getPlanActions()) {
            if (planActions.getStatutPlanAction().equals("En Cours")){
                p=planActions;
            }
        }
        if (p.getIdPlanAction()!=null) {
            Collection<Axes> a = p.getAxes();
            Collection<Objectifs> o = new HashSet<>();
            Collection<Activites> activites = new HashSet<>();
            for (Axes axes : a) {
                o.addAll(axes.getObjectifs());
            }
            for (Objectifs objectifs : o) {
                activites.addAll(objectifs.getActivites());
            }
            return activites;
        } else return null;
    }

    @Override
    public Collection<Activites> getActivitesByInstanceStatut(Long idInstance, String statut) {
        if(getActivitesByInstance(idInstance)!=null){
            Collection<Activites> a = getActivitesByInstance(idInstance);
            Collection<Activites> activites = new HashSet<>();
            for (Activites ac : a) {
                if (ac.getStatutActivite().equals(statut)) {
                    activites.add(ac);
                }
            }
            return activites;
        }else return null;
    }
}
