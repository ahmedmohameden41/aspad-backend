package mr.gov.aspadbackend.statistiques.controllers;
import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.statistiques.services.StatistiquesService;
import mr.gov.aspadbackend.suivi_activites.entites.Taches;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/statistiques")
public class StatistiquesController {
    @Autowired
    private StatistiquesService statistiquesService;

    @GetMapping(path = "/collaborateurs/{idInstance}")
    public Collection<Collaborateurs> getCollaborateursByInstance(@PathVariable(name = "idInstance") Long idInstance){
        return statistiquesService.getCollaborateursByInstance(idInstance);
    }

    @GetMapping(path = "/activites/{idInstance}")
    public Collection<Activites> getActivitesByInstance(@PathVariable(name = "idInstance") Long idInstance){
        return statistiquesService.getActivitesByInstance(idInstance);
    }

    @GetMapping(path = "/taches/{idInstance}")
    public Collection<Taches> getTachesByInstance(@PathVariable(name = "idInstance") Long idInstance){
        return statistiquesService.getTachesByInstance(idInstance);
    }

    @GetMapping(path = "/activites/{idInstance}/{statut}")
    public Collection<Activites> getActivitesByInstanceStatut(@PathVariable(name = "idInstance") Long idInstance, @PathVariable(name = "statut") String statut){
        return statistiquesService.getActivitesByInstanceStatut(idInstance, statut);
    }

    @GetMapping(path = "/taches/{idInstance}/{statut}")
    public Collection<Taches> getTachesByInstanceStatut(@PathVariable(name = "idInstance") Long idInstance, @PathVariable(name = "statut") String statut){
        return statistiquesService.getTachesByInstanceStatut(idInstance, statut);
    }

}
