package mr.gov.aspadbackend.organigramme.services;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.entities.Entites;
import mr.gov.aspadbackend.organigramme.entities.Fonctions;
import mr.gov.aspadbackend.organigramme.entities.Instances;
import mr.gov.aspadbackend.organigramme.repositories.CollaborateursRepository;
import mr.gov.aspadbackend.organigramme.repositories.EntitesRepository;
import mr.gov.aspadbackend.organigramme.repositories.FonctionsRepository;
import mr.gov.aspadbackend.organigramme.repositories.InstancesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class OrganigrammeServiceImpl implements OrganigrammeService {
    @Autowired
    private CollaborateursRepository collaborateursRepository;
    @Autowired
    private EntitesRepository entitesRepository;
    @Autowired
    private FonctionsRepository fonctionsRepository;
    @Autowired
    private InstancesRepository instancesRepository;


    @Override
    public Collaborateurs addNewCollaborateurs(Collaborateurs collaborateurs, Long idEntite, Long idFoncton) {
        if(idEntite!=null && idFoncton!=null) {
            Entites e = entitesRepository.findById(idEntite).get();
            Fonctions f = fonctionsRepository.findById(idFoncton).get();
            collaborateurs.setEntites(e);
            collaborateurs.setFonctions(f);
        }
        return collaborateursRepository.save(collaborateurs);
    }

    @Override
    public List<Collaborateurs> getCollaborateursByEntite(Long idEntite) {
        Entites e=entitesRepository.findById(idEntite).get();
        return collaborateursRepository.findAllByEntites(e);
    }

    @Override
    public Collaborateurs updateCollaborateurs(Collaborateurs collaborateurs, Long idCollaborateur, Long idEntite, Long idFoncton) {
        Entites e = entitesRepository.findById(idEntite).get();
        Fonctions f=fonctionsRepository.findById(idFoncton).get();
        collaborateurs.setIdCollaborateur(idCollaborateur);
        collaborateurs.setEntites(e);
        collaborateurs.setFonctions(f);
        return collaborateursRepository.save(collaborateurs);
    }

    @Override
    public Collaborateurs getCollaborateursByEmail(String email) {
        return collaborateursRepository.findByEmailCollaborateur(email);
    }

    @Override
    public Collaborateurs updateCollaborateurManager(Long idCollaborateur, boolean manager) {
        Collaborateurs c=collaborateursRepository.findById(idCollaborateur).get();
        System.out.println(manager);
        c.setManager(manager);
        return collaborateursRepository.save(c);
    }

    @Override
    public Entites addNewEntites(Entites entites, Long idInstance) {
        Instances i=instancesRepository.findById(idInstance).get();
        entites.setInstances(i);
        return entitesRepository.save(entites);
    }

    @Override
    public List<Entites> getEntitesByInstance(Long idInstance) {
        Instances i=instancesRepository.findById(idInstance).get();
        return entitesRepository.findAllByInstances(i);
    }

    @Override
    public Entites getEntite(Long idEntite) {
        return  entitesRepository.findById(idEntite).get();
    }

    @Override
    public void deleteEntites(Long idEntite) {
        Entites e=entitesRepository.findById(idEntite).get();
        Collection<Entites> fis = e.getEntitesFils();
        if (fis.isEmpty()){
            entitesRepository.deleteById(idEntite);
        }
    }

    @Override
    public Entites updateEntites(Entites entites, Long idEntite, Long idInstance) {
        Instances i=instancesRepository.findById(idInstance).get();
        entites.setIdEntite(idEntite);
        entites.setInstances(i);
        return entitesRepository.save(entites);
    }

    @Override
    public Fonctions getCollaborateurFonction(Long idCollaborateur) {
        Collaborateurs c = collaborateursRepository.findById(idCollaborateur).get();
        return fonctionsRepository.findByCollaborateurs(c);
    }

    @Override
    public Fonctions updateFonctions(Fonctions fonction, Long idFonction) {
        fonction.setIdFonction(idFonction);
        return fonctionsRepository.save(fonction);
    }

    @Override
    public void deleteInstances(Long idInstance) {
        instancesRepository.deleteById(idInstance);
    }

    @Override
    public Instances getInstance(Long idInstance) {
        return instancesRepository.findById(idInstance).get();
    }

    @Override
    public Instances updateInstances(Instances instances, Long idInstance) {
        instances.setIdInstance(idInstance);
        return instancesRepository.save(instances);
    }

}
