package mr.gov.aspadbackend.organigramme.services;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.entities.Entites;
import mr.gov.aspadbackend.organigramme.entities.Fonctions;
import mr.gov.aspadbackend.organigramme.entities.Instances;

import java.util.List;

public interface OrganigrammeService {

    Collaborateurs addNewCollaborateurs(Collaborateurs collaborateurs, Long idEntite, Long idFoncton);
    List<Collaborateurs> getCollaborateursByEntite(Long idEntite);
    Collaborateurs updateCollaborateurs(Collaborateurs collaborateurs, Long idCollaborateur, Long idEntite, Long idFoncton);
    Collaborateurs getCollaborateursByEmail(String email);
    Collaborateurs updateCollaborateurManager(Long idCollaborateur, boolean manager);

    Entites addNewEntites(Entites entites, Long idInstance);
    List<Entites> getEntitesByInstance(Long idInstance);
    Entites getEntite(Long idEntite);
    void deleteEntites(Long idEntite);
    Entites updateEntites(Entites entites, Long idEntite, Long idInstance);

    Fonctions getCollaborateurFonction(Long idCollaborateur);
    Fonctions updateFonctions(Fonctions fonctions, Long idFonction);

    void deleteInstances(Long idInstance);
     Instances getInstance(Long idInstance);
     Instances updateInstances(Instances instances, Long idInstance);

}
