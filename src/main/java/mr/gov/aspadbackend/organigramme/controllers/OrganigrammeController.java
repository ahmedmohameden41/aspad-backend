package mr.gov.aspadbackend.organigramme.controllers;
import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.entities.Entites;
import mr.gov.aspadbackend.organigramme.entities.Fonctions;
import mr.gov.aspadbackend.organigramme.entities.Instances;
import mr.gov.aspadbackend.organigramme.services.OrganigrammeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class OrganigrammeController {
        @Autowired
        private OrganigrammeService organigrammeService;


        @GetMapping(path = "/instanceses/{idInstance}/entites")
        public List<Entites> getEntitesByInstance(@PathVariable(name = "idInstance") Long idInstance){
                return organigrammeService.getEntitesByInstance(idInstance);
        }
        @PostMapping (path = "/instanceses/{idInstance}/entites")
        public Entites addNewEntites(@RequestBody Entites entites, @PathVariable(name = "idInstance") Long idInstance){
                return organigrammeService.addNewEntites(entites, idInstance);
        }
        @GetMapping(path = "/entiteses/{idEntite}")
        public Entites getEntite(@PathVariable(name = "idEntite") Long idEntite){
                return organigrammeService.getEntite(idEntite);
        }
        @PutMapping(path = "/entiteses/{idEntite}/{idInstance}")
        public Entites updateEntite(@RequestBody Entites entite, @PathVariable(name = "idEntite") Long idEntite,  @PathVariable(name = "idInstance") Long idInstance){
              return organigrammeService.updateEntites(entite, idEntite, idInstance);
        }
        @DeleteMapping(value = "/entiteses/{idEntite}")
        public void deleteEntite(@PathVariable(name = "idEntite") Long idEntite){
                organigrammeService.deleteEntites(idEntite);
        }


        @GetMapping(path = "/entiteses/{idEntite}/collaborateurs")
        public List<Collaborateurs> getCollaborateursByEntite(@PathVariable(name = "idEntite") Long idEntite){
                return organigrammeService.getCollaborateursByEntite(idEntite);
        }
        @GetMapping(path = "/collaborateurses/{email}/instance")
        public Collaborateurs getCollaborateursByEmail(@PathVariable(name = "email") String email){
                return organigrammeService.getCollaborateursByEmail(email);
        }
        @PostMapping (path = "/entiteses/{idEntite}/collaborateurs/{idFonction}")
        public Collaborateurs addNewCollaborateurs(@RequestBody Collaborateurs collaborateurs, @PathVariable(name = "idEntite") Long idEntite, @PathVariable(name = "idFonction") Long idFonction){
                return organigrammeService.addNewCollaborateurs(collaborateurs, idEntite, idFonction);
        }
        @PutMapping(path = "/collaborateurses/{idCollaborateur}/{idEntite}/{idFonction}")
        public Collaborateurs updateCollaborateur(@RequestBody Collaborateurs collaborateurs, @PathVariable(name = "idCollaborateur") Long idCollaborateur, @PathVariable(name = "idEntite") Long idEntite,  @PathVariable(name = "idFonction") Long idFonction){
                return organigrammeService.updateCollaborateurs(collaborateurs, idCollaborateur, idEntite, idFonction);
        }
        @PutMapping(path = "/collaborateurses/{idCollaborateur}/{manager}/equipes")
        public Collaborateurs updateCollaborateurManager(@RequestBody Collaborateurs collaborateurs, @PathVariable(name = "idCollaborateur") Long idCollaborateur, @PathVariable(name = "manager") boolean manager){
                return organigrammeService.updateCollaborateurManager(idCollaborateur, manager);
        }
        @GetMapping(path = "/collaborateurses/{idCollaborateur}/fonction")
        public Fonctions getCollaborateurFonction(@PathVariable(name = "idCollaborateur") Long idCollaborateur){
                return organigrammeService.getCollaborateurFonction(idCollaborateur);
        }
        @PutMapping(path = "/fonctionses/{idFonction}")
        public Fonctions updateFonction(@RequestBody Fonctions fonctions, @PathVariable(name = "idFonction") Long idFonction){
                return organigrammeService.updateFonctions(fonctions, idFonction);
        }

        @GetMapping(path = "/instanceses/{idInstance}")
        public Instances getInstance(@PathVariable(name = "idInstance") Long idInstance){
                return organigrammeService.getInstance(idInstance);
        }

        @PutMapping(path = "/instanceses/{idInstance}")
        public Instances updateInstance(@RequestBody Instances instances, @PathVariable(name = "idInstance") Long idInstance){
                return organigrammeService.updateInstances(instances, idInstance);
        }

        @DeleteMapping(value = "/instanceses/{idInstance}")
        public void deleteInstance(@PathVariable(name = "idInstance") Long idInstance){
                organigrammeService.deleteInstances(idInstance);
        }

}
