package mr.gov.aspadbackend.organigramme.repositories;

import mr.gov.aspadbackend.organigramme.entities.Entites;
import mr.gov.aspadbackend.organigramme.entities.Instances;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource
@CrossOrigin("*")
public interface EntitesRepository extends JpaRepository<Entites, Long> {
   List<Entites> findAllByInstances(Instances instances);
}
