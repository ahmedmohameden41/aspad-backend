package mr.gov.aspadbackend.organigramme.repositories;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.entities.Fonctions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin("*")
public interface FonctionsRepository extends JpaRepository<Fonctions, Long> {
    Fonctions findByCollaborateurs(Collaborateurs collaborateurs);
}
