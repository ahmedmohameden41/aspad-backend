package mr.gov.aspadbackend.organigramme.repositories;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.entities.Entites;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource
@CrossOrigin( origins = "*")
public interface CollaborateursRepository extends JpaRepository<Collaborateurs, Long> {
     Collaborateurs findByEmailCollaborateur(String emailCollaborateur);
     List<Collaborateurs> findAllByEntites(Entites entites);
}
