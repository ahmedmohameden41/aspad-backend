package mr.gov.aspadbackend.organigramme.repositories;

import mr.gov.aspadbackend.organigramme.entities.Instances;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@RepositoryRestResource
@CrossOrigin(origins="*")
public interface InstancesRepository extends JpaRepository<Instances, Long> {
}
