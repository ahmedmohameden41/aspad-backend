package mr.gov.aspadbackend.organigramme.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString(onlyExplicitlyIncluded = true)
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Entites implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdEntite;
    private String DesignationEntite;
    private String AbreviationEntite;
    private String DescriptionEntite;

    @ManyToOne

	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    private Instances instances;

    @ManyToOne

	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    private Entites EntiteParent;

    @JsonIgnore
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    @OneToMany(mappedBy="EntiteParent")
    private Collection<Entites> EntitesFils;

    @JsonIgnore
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "entites",fetch = FetchType.LAZY)
    private Collection<Collaborateurs> collaborateurs;

}
