package mr.gov.aspadbackend.organigramme.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import mr.gov.aspadbackend.plan_action.entites.PlanActions;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

@Entity
@Data @EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor @AllArgsConstructor @ToString(onlyExplicitlyIncluded = true)
public class Instances implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdInstance;
    private String DesignationInstance;
    private String AbreviatonInstance;
    private String DescriptionInstance;
    private String SiteWebInstance;
    private String AdresseInstance;
    private String EmailInstance;
    private Long TelephoneInstance;
    private String FaxInstance;
    private String BPInstance;

    @JsonIgnore
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "instances", fetch = FetchType.LAZY)
    private Collection<Entites> entites;

    @JsonIgnore
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "instances", fetch = FetchType.LAZY)
    private Collection<PlanActions> planActions;
}
