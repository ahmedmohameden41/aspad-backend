package mr.gov.aspadbackend.organigramme.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.suivi_activites.entites.CommentairesActivite;
import mr.gov.aspadbackend.suivi_activites.entites.CommentairesTache;
import mr.gov.aspadbackend.suivi_activites.entites.Taches;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalIdCache;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
@Data @EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor @AllArgsConstructor @ToString(onlyExplicitlyIncluded = true)
@NaturalIdCache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Collaborateurs implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdCollaborateur;
    private Long matriculeCollaborateur;
    private String nomCollaborateur;
    private String prenomCollaborateur;
    private String emailCollaborateur;
    private Long telephoneCollaborateur;
    @Temporal(TemporalType.DATE)
    private Date dateNaisCollaborateur;
    private String lieuNaisCollaborateur;
    private String adresseCollaborateur;
    private String photoCollaborateur;
    private boolean user;
    private boolean manager;

	@JsonIgnore
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    @ManyToOne
    private Fonctions fonctions;

	@JsonIgnore
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    @ManyToOne
    private Entites entites;

	@JsonIgnore
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    @ManyToOne
    private Activites activites;

	@JsonIgnore
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "collaborateurs",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<CommentairesActivite> commentairesActivites;

	@JsonIgnore
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "collaborateurs",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Collection<CommentairesTache> commentairesTaches;

	@JsonIgnore
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "collaborateurs", fetch = FetchType.LAZY)
    private Collection<Taches> taches = new HashSet<>();
}
