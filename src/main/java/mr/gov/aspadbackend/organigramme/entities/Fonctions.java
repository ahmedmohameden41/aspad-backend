package mr.gov.aspadbackend.organigramme.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import mr.gov.aspadbackend.suivi_activites.entites.CommentairesActivite;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Fonctions implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdFonction;
    private String DesignationFonction;
    private String AbreviationFonction;
//    @JsonManagedReference
    @OneToMany(mappedBy = "fonctions")
    @JsonIgnore
    private Collection<Collaborateurs> collaborateurs;
	public Long getIdFonction() {
		return IdFonction;
	}
	public void setIdFonction(Long idFonction) {
		IdFonction = idFonction;
	}
	public String getDesignationFonction() {
		return DesignationFonction;
	}
	public void setDesignationFonction(String designationFonction) {
		DesignationFonction = designationFonction;
	}
	public String getAbreviationFonction() {
		return AbreviationFonction;
	}
	public void setAbreviationFonction(String abreviationFonction) {
		AbreviationFonction = abreviationFonction;
	}
	public Collection<Collaborateurs> getCollaborateurs() {
		return collaborateurs;
	}
	public void setCollaborateurs(Collection<Collaborateurs> collaborateurs) {
		this.collaborateurs = collaborateurs;
	}
    
}
