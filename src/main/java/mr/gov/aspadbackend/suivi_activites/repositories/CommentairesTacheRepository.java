package mr.gov.aspadbackend.suivi_activites.repositories;

import mr.gov.aspadbackend.suivi_activites.entites.CommentairesTache;
import mr.gov.aspadbackend.suivi_activites.entites.Taches;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;

@RepositoryRestResource
@CrossOrigin(origins = "*")
public interface CommentairesTacheRepository extends JpaRepository<CommentairesTache, Long> {
    Collection<CommentairesTache> findAllByTaches(Taches taches);
}
