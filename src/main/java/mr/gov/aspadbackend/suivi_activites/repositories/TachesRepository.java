package mr.gov.aspadbackend.suivi_activites.repositories;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.suivi_activites.entites.Taches;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.Collection;

@RepositoryRestResource
@CrossOrigin(origins = "*")
public interface TachesRepository extends JpaRepository<Taches, Long> {
    Collection<Taches> findByCollaborateursAndActivites(Collaborateurs collaborateurs, Activites activites);
}
