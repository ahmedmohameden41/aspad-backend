package mr.gov.aspadbackend.suivi_activites.repositories;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.suivi_activites.entites.CommentairesActivite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;

@RepositoryRestResource
@CrossOrigin(origins = "*")
public interface CommentairesActiviteRepository extends JpaRepository<CommentairesActivite, Long> {
}
