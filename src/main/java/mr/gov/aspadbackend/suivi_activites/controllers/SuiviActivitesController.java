package mr.gov.aspadbackend.suivi_activites.controllers;
import mr.gov.aspadbackend.suivi_activites.entites.CommentairesActivite;
import mr.gov.aspadbackend.suivi_activites.entites.CommentairesTache;
import mr.gov.aspadbackend.suivi_activites.entites.Taches;
import mr.gov.aspadbackend.suivi_activites.services.SuiviActivitesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class SuiviActivitesController {
    @Autowired
    private SuiviActivitesService suiviActivitesService;

    @GetMapping(path = "/tacheses/{idActivite}/{idCollaborateur}")
    public Collection<Taches> getTachesByCollaborateurActivite(@PathVariable(name = "idActivite") Long idActivite, @PathVariable(name = "idCollaborateur") Long idCollaborateur) {
        return suiviActivitesService.getTachesByCollaborateurActivite(idActivite, idCollaborateur);
    }
    @GetMapping(path = "/tacheses/{idTahce}/commentairesTaches")
    public Collection<CommentairesTache> getCommentairesTaches(@PathVariable(name = "idTahce") Long idTahce) {
        return suiviActivitesService.getCommentairesTaches(idTahce);
    }

    @PostMapping(path = "/tacheses/{idActivite}/{idCollaborateur}")
    public Taches addNewTaches(@RequestBody Taches taches, @PathVariable(name = "idActivite") Long idActivite, @PathVariable(name = "idCollaborateur") Long idCollaborateur){
        return suiviActivitesService.addNewTaches(taches, idActivite, idCollaborateur);
    }
    @PutMapping(path = "/tacheses/{idTache}/{idActivite}/{idCollaborateur}")
    public Taches updateActivites(@RequestBody Taches taches, @PathVariable(name = "idTache") Long idTache, @PathVariable(name = "idActivite") Long idActivite, @PathVariable(name = "idCollaborateur") Long idCollaborateur){
        return suiviActivitesService.updateTaches(taches, idTache, idActivite, idCollaborateur);
    }

    @PostMapping(path = "/commentairesActivites/{idActivite}/{idCollaborateur}")
    public CommentairesActivite addNewCommentairesActivite(@RequestBody CommentairesActivite commentairesActivite, @PathVariable(name = "idActivite") Long idActivite, @PathVariable(name = "idCollaborateur") Long idCollaborateur){
        return suiviActivitesService.addNewCommentairesActivite(commentairesActivite, idActivite, idCollaborateur);
    }
    @PostMapping(path = "/commentairesTaches/{idTache}/{idCollaborateur}")
    public CommentairesTache addNewCommentairesTache(@RequestBody CommentairesTache commentairesTache, @PathVariable(name = "idTache") Long idTache, @PathVariable(name = "idCollaborateur") Long idCollaborateur){
        return suiviActivitesService.addNewCommentairesTache(commentairesTache, idTache, idCollaborateur);
    }
}
