package mr.gov.aspadbackend.suivi_activites.services;

import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.organigramme.repositories.CollaborateursRepository;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import mr.gov.aspadbackend.plan_action.repositories.ActivitesRepository;
import mr.gov.aspadbackend.suivi_activites.entites.*;
import mr.gov.aspadbackend.suivi_activites.repositories.CommentairesActiviteRepository;
import mr.gov.aspadbackend.suivi_activites.repositories.CommentairesTacheRepository;
import mr.gov.aspadbackend.suivi_activites.repositories.TachesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class SuiviActivitesServiceImpl implements SuiviActivitesService {
    @Autowired
    private TachesRepository tachesRepository;
    @Autowired
    private CommentairesActiviteRepository commentairesActiviteRepository;
    @Autowired
    private CommentairesTacheRepository commentairesTacheRepository;
    @Autowired
    private ActivitesRepository activitesRepository;
    @Autowired
    private CollaborateursRepository collaborateursRepository;

    @Override
    public CommentairesTache addNewCommentairesTache(CommentairesTache commentairesTache, Long idTache, Long idCollaborateur) {
        Collaborateurs c = collaborateursRepository.findById(idCollaborateur).get();
        commentairesTache.setCollaborateurs(c);
        commentairesTache.setTaches(tachesRepository.findById(idTache).get());
        commentairesTache.setDateCommentaireTache(new Date());
        commentairesTache.setAuteurTache(c.getNomCollaborateur()+" "+c.getPrenomCollaborateur());
        return commentairesTacheRepository.save(commentairesTache);
    }

    @Override
    public CommentairesActivite addNewCommentairesActivite(CommentairesActivite commentairesActivite, Long idActivite, Long idCollaborateur) {
        Collaborateurs c = collaborateursRepository.findById(idCollaborateur).get();
        commentairesActivite.setCollaborateurs(c);
        commentairesActivite.setActivites(activitesRepository.findById(idActivite).get());
        commentairesActivite.setDateCommentaireActivite(new Date());
        commentairesActivite.setAuteurActivite(c.getNomCollaborateur()+" "+c.getPrenomCollaborateur());
        return commentairesActiviteRepository.save(commentairesActivite);
    }

    @Override
    public Taches addNewTaches(Taches taches, Long idActivite, Long idCollaborateur) {
        if (idActivite != null && idCollaborateur != null) {
            taches.setCollaborateurs(collaborateursRepository.findById(idCollaborateur).get());
            taches.setActivites(activitesRepository.findById(idActivite).get());
            return tachesRepository.save(taches);
        }
        else if(idCollaborateur != null){
            taches.setCollaborateurs(collaborateursRepository.findById(idCollaborateur).get());
            return tachesRepository.save(taches);
        }else {
            taches.setActivites(activitesRepository.findById(idActivite).get());
            return tachesRepository.save(taches);
        }
    }

    @Override
    public Collection<Taches> getTachesByCollaborateurActivite(Long idActivite, Long idCollaborateur) {
        if (idActivite != null && idCollaborateur != null) {
            Activites a = activitesRepository.findById(idActivite).get();
            Collaborateurs c = collaborateursRepository.findById(idCollaborateur).get();
            return tachesRepository.findByCollaborateursAndActivites(c, a);
        }else if(idCollaborateur != null){
            Collaborateurs c = collaborateursRepository.findById(idCollaborateur).get();
            return c.getTaches();
        }else {
            Activites a = activitesRepository.findById(idActivite).get();
            return a.getTaches();
        }
    }

    @Override
    public Collection<CommentairesTache> getCommentairesTaches(Long idTahce) {
        Taches taches = tachesRepository.findById(idTahce).get();
        return commentairesTacheRepository.findAllByTaches(taches);
    }

    @Override
    public Taches updateTaches(Taches taches, Long idTache, Long idActivite, Long idCollaborateur) {
        Activites a = activitesRepository.findById(idActivite).get();
        Collaborateurs c = collaborateursRepository.findById(idCollaborateur).get();
        if (idTache != null && idActivite != null && idCollaborateur != null) {
            taches.setCollaborateurs(c);
            taches.setActivites(a);
            taches.setIdTache(idTache);
            return tachesRepository.save(taches);
        }else {
            return null;
        }
    }
}
