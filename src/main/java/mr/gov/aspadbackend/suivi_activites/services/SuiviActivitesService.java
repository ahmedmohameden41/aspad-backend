package mr.gov.aspadbackend.suivi_activites.services;

import mr.gov.aspadbackend.suivi_activites.entites.CommentairesActivite;
import mr.gov.aspadbackend.suivi_activites.entites.CommentairesTache;
import mr.gov.aspadbackend.suivi_activites.entites.Taches;

import java.util.Collection;

public interface SuiviActivitesService {
    CommentairesTache addNewCommentairesTache(CommentairesTache commentairesTache, Long idTache, Long idCollaborateur);

    CommentairesActivite addNewCommentairesActivite(CommentairesActivite commentairesActivite, Long idActivite, Long idCollaborateur);

    Taches addNewTaches(Taches taches, Long idActivite, Long idCollaborateur);
    Collection<Taches> getTachesByCollaborateurActivite(Long idActivite, Long idCollaborateur);
    Taches updateTaches(Taches taches, Long idTache, Long idActivite, Long idCollaborateur);

    Collection<CommentairesTache> getCommentairesTaches(Long idTahce);
}
