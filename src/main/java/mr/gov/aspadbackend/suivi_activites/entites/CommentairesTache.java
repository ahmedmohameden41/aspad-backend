package mr.gov.aspadbackend.suivi_activites.entites;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class CommentairesTache{
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdCommentairesTache;

    @ManyToOne
    private Collaborateurs collaborateurs;

    @ManyToOne
    private Taches taches;

    private Date DateCommentaireTache;
    private String CommentaireTache;
    private String auteurTache;
	public Long getIdCommentairesTache() {
		return IdCommentairesTache;
	}
	public void setIdCommentairesTache(Long idCommentairesTache) {
		IdCommentairesTache = idCommentairesTache;
	}
	public Collaborateurs getCollaborateurs() {
		return collaborateurs;
	}
	public void setCollaborateurs(Collaborateurs collaborateurs) {
		this.collaborateurs = collaborateurs;
	}
	public Taches getTaches() {
		return taches;
	}
	public void setTaches(Taches taches) {
		this.taches = taches;
	}
	public Date getDateCommentaireTache() {
		return DateCommentaireTache;
	}
	public void setDateCommentaireTache(Date dateCommentaireTache) {
		DateCommentaireTache = dateCommentaireTache;
	}
	public String getCommentaireTache() {
		return CommentaireTache;
	}
	public void setCommentaireTache(String commentaireTache) {
		CommentaireTache = commentaireTache;
	}
	public String getAuteurTache() {
		return auteurTache;
	}
	public void setAuteurTache(String auteurTache) {
		this.auteurTache = auteurTache;
	}

}
