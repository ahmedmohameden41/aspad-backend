package mr.gov.aspadbackend.suivi_activites.entites;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.plan_action.entites.Activites;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class CommentairesActivite{
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdCommentairesActivite;

    @ManyToOne(fetch = FetchType.LAZY)
    private Collaborateurs collaborateurs;

    @ManyToOne(fetch = FetchType.LAZY)
    private Activites activites;

    private Date DateCommentaireActivite;
    private String CommentaireActivite;
    private String auteurActivite;
	public Long getIdCommentairesActivite() {
		return IdCommentairesActivite;
	}
	public void setIdCommentairesActivite(Long idCommentairesActivite) {
		IdCommentairesActivite = idCommentairesActivite;
	}
	public Collaborateurs getCollaborateurs() {
		return collaborateurs;
	}
	public void setCollaborateurs(Collaborateurs collaborateurs) {
		this.collaborateurs = collaborateurs;
	}
	public Activites getActivites() {
		return activites;
	}
	public void setActivites(Activites activites) {
		this.activites = activites;
	}
	public Date getDateCommentaireActivite() {
		return DateCommentaireActivite;
	}
	public void setDateCommentaireActivite(Date dateCommentaireActivite) {
		DateCommentaireActivite = dateCommentaireActivite;
	}
	public String getCommentaireActivite() {
		return CommentaireActivite;
	}
	public void setCommentaireActivite(String commentaireActivite) {
		CommentaireActivite = commentaireActivite;
	}
	public String getAuteurActivite() {
		return auteurActivite;
	}
	public void setAuteurActivite(String auteurActivite) {
		this.auteurActivite = auteurActivite;
	}
    
}

