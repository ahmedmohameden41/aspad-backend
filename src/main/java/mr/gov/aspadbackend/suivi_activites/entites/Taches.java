package mr.gov.aspadbackend.suivi_activites.entites;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import mr.gov.aspadbackend.organigramme.entities.Collaborateurs;
import mr.gov.aspadbackend.plan_action.entites.Activites;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NaturalIdCache;
import org.hibernate.annotations.Cache;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
@NaturalIdCache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class Taches implements Serializable {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdTache;
    private String DesignationTache;
    private String DescriptionTache;
    private String ObservationTache;
    private String StatutTache;
    @Temporal(TemporalType.DATE)
    private Date DateDebutTache;
    @Temporal(TemporalType.DATE)
    private Date DateFinTache;
    @Temporal(TemporalType.DATE)
    private Date DateDebutPrevueTache;
    @Temporal(TemporalType.DATE)
    private Date DateFinPrevueTache;
//    @Length(max = 3)
    private int PourcentageTache;

    @ManyToOne
    private Activites activites;

    @ManyToOne
    private Collaborateurs collaborateurs;

    @OneToMany(mappedBy = "taches",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Collection<CommentairesTache> commentairesTaches;

	public Long getIdTache() {
		return IdTache;
	}

	public void setIdTache(Long idTache) {
		IdTache = idTache;
	}

	public String getDesignationTache() {
		return DesignationTache;
	}

	public void setDesignationTache(String designationTache) {
		DesignationTache = designationTache;
	}

	public String getDescriptionTache() {
		return DescriptionTache;
	}

	public void setDescriptionTache(String descriptionTache) {
		DescriptionTache = descriptionTache;
	}

	public String getObservationTache() {
		return ObservationTache;
	}

	public void setObservationTache(String observationTache) {
		ObservationTache = observationTache;
	}

	public String getStatutTache() {
		return StatutTache;
	}

	public void setStatutTache(String statutTache) {
		StatutTache = statutTache;
	}

	public Date getDateDebutTache() {
		return DateDebutTache;
	}

	public void setDateDebutTache(Date dateDebutTache) {
		DateDebutTache = dateDebutTache;
	}

	public Date getDateFinTache() {
		return DateFinTache;
	}

	public void setDateFinTache(Date dateFinTache) {
		DateFinTache = dateFinTache;
	}

	public Date getDateDebutPrevueTache() {
		return DateDebutPrevueTache;
	}

	public void setDateDebutPrevueTache(Date dateDebutPrevueTache) {
		DateDebutPrevueTache = dateDebutPrevueTache;
	}

	public Date getDateFinPrevueTache() {
		return DateFinPrevueTache;
	}

	public void setDateFinPrevueTache(Date dateFinPrevueTache) {
		DateFinPrevueTache = dateFinPrevueTache;
	}

	public int getPourcentageTache() {
		return PourcentageTache;
	}

	public void setPourcentageTache(int pourcentageTache) {
		PourcentageTache = pourcentageTache;
	}

	public Activites getActivites() {
		return activites;
	}

	public void setActivites(Activites activites) {
		this.activites = activites;
	}

	public Collaborateurs getCollaborateurs() {
		return collaborateurs;
	}

	public void setCollaborateurs(Collaborateurs collaborateurs) {
		this.collaborateurs = collaborateurs;
	}

	public Collection<CommentairesTache> getCommentairesTaches() {
		return commentairesTaches;
	}

	public void setCommentairesTaches(Collection<CommentairesTache> commentairesTaches) {
		this.commentairesTaches = commentairesTaches;
	}
    
    
}
